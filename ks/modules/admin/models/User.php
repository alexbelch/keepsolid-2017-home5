<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $ID
 * @property string $NameFirst
 * @property string $NameLast
 *
 * @property Useremail $useremail
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NameFirst', 'NameLast'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NameFirst' => 'Name First',
            'NameLast' => 'Name Last',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUseremail()
    {
        return $this->hasOne(Useremail::className(), ['UserId' => 'ID']);
    }
}
